#ifndef TERMINAL_H
#define TERMINAL_H

#include "types.h"
#include "color.h"

class Terminal
{
private:
    uint32_t row;
    uint32_t column;
    uint8_t color;
    uint16_t * buffer;
private:
    void putentryat(char c, uint16_t color, uint32_t x, uint32_t y);
    void putChar(char c);

public:
    Terminal();
    void initialize();
    void setColor(uint8_t color);
    void writeString(const char * data);
};

#endif