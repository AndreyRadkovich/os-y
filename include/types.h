#ifndef TYPES_H
#define TYPES_H

static const unsigned int VGA_WIDTH = 80;
static const unsigned int VGA_HEIGHT = 25;

typedef unsigned char uint8_t;
typedef char int8_t;
typedef unsigned short uint16_t;
typedef short int16_t;
typedef unsigned int uint32_t;
typedef int int32_t;
typedef unsigned long uint64_t;
typedef long int64_t;

#endif
