#include "../include/terminal.h"

unsigned int strlen(const char * str) {

    unsigned int length = 0;
    while(str[length] != 0) length++;
    return length;
}

Terminal::Terminal(){}

void Terminal::initialize() {

    row = 0;
    column = 0;
    Color _color;
    color = _color.makeColor(Color::COLOR_DARK_GREY , Color::COLOR_BLACK);
    buffer = (uint16_t *) 0xB8000;
    for(uint32_t i = 0; i < VGA_HEIGHT; i++)
    {
        for(uint32_t j = 0; j < VGA_WIDTH; j++)
        {
            const uint32_t index = i * VGA_HEIGHT + j;
            buffer[index] = _color.makeVGAEntry(' ', color);
        }
    }
}

void Terminal::setColor(uint8_t _color) {

    color = _color;
}

void Terminal::putentryat(char c, uint16_t color, uint32_t x, uint32_t y) {

    const uint32_t index = y * VGA_WIDTH + x;
    Color _color;
    buffer[index] = _color.makeVGAEntry(c, color);
}

void Terminal::putChar(char c) {

    putentryat(c, color, column, row);
    if ( ++column == VGA_WIDTH )
    {
        column = 0;
        if ( ++row == VGA_HEIGHT )
        {
            row = 0;
        }
    }
}

void Terminal::writeString(const char * data) {

    for(uint32_t i = 0; i < strlen(data); i++)
    {
        putChar(data[i]);
    }
}
