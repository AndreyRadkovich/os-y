//#include "color.h"
#include "../include/color.h"

Color::Color() {}

uint8_t Color::makeColor(uint8_t fg, uint8_t bg) {

    return fg | bg << 4;
}

uint16_t Color::makeVGAEntry(uint8_t c, uint8_t color) {

    uint16_t c16 = c;
    uint16_t color16 = color;
    return c16 | color16 << 8;
}
