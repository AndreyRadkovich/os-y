#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif

#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif

#include "../include/terminal.h"

#if defined(__cplusplus)
extern "C"
#endif

void kernel_main() {

    Terminal terminal;
    terminal.initialize();
    terminal.writeString("Hello, kernel_main!\n");
}
